package football.data.svc.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class Player {
	@JsonProperty("player_key")
	public long player_key;
	
	@JsonProperty("player_name")
	public String player_name;
	
	@JsonProperty("player_number")
	public String player_number;
	
	@JsonProperty("player_country")
	public String player_country;
	
	@JsonProperty("player_type")
	public String player_type;
	
	@JsonProperty("player_age")
	public String player_age;
	
	@JsonProperty("player_match_played")
	public String player_match_played;
	
	@JsonProperty("player_goals")
	public String player_goals;
	
	@JsonProperty("player_yellow_cards")
	public String player_yellow_cards;
	
	@JsonProperty("player_red_cards")
	public String player_red_cards;

}
