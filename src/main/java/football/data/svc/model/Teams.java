package football.data.svc.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class Teams {
	@JsonProperty("team_key")
	public String team_key;
	
	@JsonProperty("team_name")
	public String team_name;
	
	@JsonProperty("team_badge")
	public String team_badge;
	
	@JsonProperty("players")
	public List<Player> players;
	
	@JsonProperty("coaches")
	public List<Coach> coaches;
}
