package football.data.svc.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class LeagueData  {
	
	@JsonProperty("country_id")
	private String country_id;
	
	@JsonProperty("country_name")
	private String country_name;
	
	@JsonProperty("league_id")
	private String league_id;
	
	@JsonProperty("league_name")
	private String league_Name;

	public String getCountry_id() {
		return country_id;
	}

	public void setCountry_id(String country_id) {
		this.country_id = country_id;
	}

	public String getCountry_name() {
		return country_name;
	}

	public String getLeague_id() {
		return league_id;
	}

	public void setLeague_id(String league_id) {
		this.league_id = league_id;
	}

	public String getLeague_Name() {
		return league_Name;
	}

	public void setLeague_Name(String league_Name) {
		this.league_Name = league_Name;
	}

	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}
	
}
