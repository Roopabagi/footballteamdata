package football.data.svc.utils;

import java.net.URI;
import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import football.data.svc.model.CountryData;
import football.data.svc.model.FootBallRequestParam;
import football.data.svc.model.LeagueData;
import football.data.svc.model.Standings;
import football.data.svc.model.Teams;

@Component
public class RestClientAction {

	private RestTemplate restTemplate;
	private static final String getCountry_action = "get_countries";
	private static final String getTeams_action = "get_teams";
	private static final String getStanding_action = "get_standings";

	public RestClientAction(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	/**
	 * 
	 * @param reqParam
	 * @return
	 */
	public Object execute(FootBallRequestParam reqParam) {
		HttpEntity<String> entity = prepareHttpEntity(reqParam);
		URI uri = prepareURI(reqParam);
		if ( reqParam.getAction() != null) {
			if (reqParam.getAction().equals(getCountry_action)) {
				return restTemplate.exchange(uri, HttpMethod.GET, entity, CountryData[].class).getBody();

			} else if (reqParam.getAction().equals(getTeams_action)) {
				return restTemplate.exchange(uri, HttpMethod.GET, entity, Teams[].class).getBody();
			} else if (reqParam.getAction().equals(getStanding_action)) {
				Object temp = restTemplate.exchange(uri, HttpMethod.GET, entity, Standings[].class).getBody();
				return temp;
			} else
				return restTemplate.exchange(uri, HttpMethod.GET, entity, LeagueData[].class).getBody();
		}
		return null;
	}

	/**
	 * 
	 * @param reqParam
	 * @return
	 */
	private URI prepareURI(FootBallRequestParam reqParam) {
		UriComponentsBuilder uribuilder = null;
		URI uri = null;
		String temp = "https://apiv2.apifootball.com/?";

		uribuilder = UriComponentsBuilder.fromUriString(temp).queryParam("action", reqParam.getAction())
				.queryParam("APIkey", "9bb66184e0c8145384fd2cc0f7b914ada57b4e8fd2e4d6d586adcc27c257a978");
		if(reqParam.getAction() != null) {
		if (!strIsBlankOrEmpty(reqParam.getAction()) && reqParam.getAction().equals(getCountry_action)) {
			uribuilder.queryParam("country_id", reqParam.getCountry_id());
		} else if (!strIsBlankOrEmpty(reqParam.getAction()) && reqParam.getAction().equals(getTeams_action)) {

			if (!strIsBlankOrEmpty(reqParam.getTeam_id()))
				uribuilder.queryParam("team_id", reqParam.getTeam_id());

			if (!strIsBlankOrEmpty(reqParam.getLeague_id()))
				uribuilder.queryParam("league_id", reqParam.getLeague_id());

		} else if (reqParam.getAction().equals(getStanding_action)) {

			if (!strIsBlankOrEmpty(reqParam.getLeague_id()))
				uribuilder.queryParam("league_id", reqParam.getLeague_id());
		}
		}
		return uribuilder.build().toUri();
	}

	private boolean strIsBlankOrEmpty(String input) {
		if (input == null || input.isEmpty())
			return true;
		else
			return false;
	}

	private HttpEntity<String> prepareHttpEntity(FootBallRequestParam action) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("action", action.getAction());
		headers.add("APIkey", "9bb66184e0c8145384fd2cc0f7b914ada57b4e8fd2e4d6d586adcc27c257a978");
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		return entity;
	}

}
