package football.data.svc.service;

import java.util.Arrays;

import org.apache.tomcat.util.json.ParseException;
import org.springframework.stereotype.Service;

import football.data.svc.model.CountryData;
import football.data.svc.model.FootBallRequestParam;
import football.data.svc.model.LeagueData;
import football.data.svc.model.Standings;
import football.data.svc.model.Teams;
import football.data.svc.utils.RestClientAction;

/**
 * 
 * @author Roopa B
 *
 */
@Service
public class FootBallTeamDataService {

	
	RestClientAction restClient;

	public FootBallTeamDataService(RestClientAction restClient) {
		this.restClient = restClient;
	}

	/**
	 * 
	 * @param action
	 * @param countryID
	 * @return
	 */
	public CountryData[] getCountryData(String action, String countryID) {
		FootBallRequestParam reqParam = new FootBallRequestParam();
		reqParam.setAction(action);
		reqParam.setCountry_id(countryID);
		return (CountryData[]) restClient.execute(reqParam);

	}

	/**
	 * 
	 * @param action
	 * @return
	 */
	public LeagueData[] getLeagueData(String action) {
		FootBallRequestParam reqParam = new FootBallRequestParam();
		reqParam.setAction(action);
		return (LeagueData[]) restClient.execute(reqParam);
	}

	/**
	 * 
	 * @param action
	 * @param teamId
	 * @param leagueId
	 * @return
	 */
	public Teams[] getTeams(String action, String teamId, String leagueId) {
		FootBallRequestParam reqParam = new FootBallRequestParam();
		reqParam.setAction(action);
		reqParam.setTeam_id(teamId);
		reqParam.setLeague_id(leagueId);
		return (Teams[]) restClient.execute(reqParam);
	}

	/**
	 * 
	 * @param action
	 * @param leagueId
	 * @return
	 * @throws ParseException
	 */
	public Standings[] getStandings(String action, String leagueId) throws ParseException {
		FootBallRequestParam reqParam = new FootBallRequestParam();
		reqParam.setAction(action);
		reqParam.setLeague_id(leagueId);
		Standings[] result = (Standings[]) restClient.execute(reqParam);
		// List<String> countryDetail = Arrays.stream(result).
		// filter(element -> element.getCountry_name() != null).map(element ->
		// element.getCountry_name()).collect(Collectors.toList());
		CountryData[] contry = null;
		if(result != null) {
			contry = getCountryData("get_countries", null);
		for (Standings standing : result) {
			for (CountryData data : contry) {
				if (standing.getCountry_name().equals(data.getCountry_name())) {
					standing.setCountry_id(data.getCountry_id());
				}
			}
		}
		}
		return result;

	}

}
