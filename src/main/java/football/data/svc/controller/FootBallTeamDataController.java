package football.data.svc.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.tomcat.util.json.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import football.data.svc.model.CountryData;
import football.data.svc.model.LeagueData;
import football.data.svc.model.Standings;
import football.data.svc.model.Teams;
import football.data.svc.service.FootBallTeamDataService;

/**
 * 
 * @author Roopa.B
 *
 */
@RestController
@RequestMapping("/api")
public class FootBallTeamDataController {

	@Autowired
	private FootBallTeamDataService footBallTeamDataService;

	/**
	 * 
	 * @param country_id GET apiv2.apifootball.com/?action=get_countries
	 * @return list of supported countries included in your current subscription
	 *         plan
	 */
	@GetMapping(value = "/getCountries")
	public List<CountryData> getCountries(@RequestParam(required = false) String country_id) {
		CountryData[] data = footBallTeamDataService.getCountryData("get_countries", country_id);
		List<CountryData> result = null;
		if (!Objects.isNull(data)) {
			if (!Objects.isNull(country_id)) {
				result = Arrays.stream(data).filter(element -> element.getCountry_id().equals(country_id))
						.collect(Collectors.toList());
			} else {
				result = Arrays.stream(data).collect(Collectors.toList());
			}
		}
		return result;
	}

	/**
	 * GET apiv2.apifootball.com/?action=get_leagues
	 * 
	 * @return list of supported competitions included in your current subscription
	 *         plan
	 */
	@GetMapping(value = "/getLeague")
	public LeagueData[] getLeagueData() {
		return footBallTeamDataService.getLeagueData("get_leagues");
	}

	/**
	 * 
	 * @param team_id
	 * @param league_id GET apiv2.apifootball.com/?action=get_teams
	 * @return list of available teams
	 */
	@GetMapping(value = "/getTeams")
	public Teams[] getTeamsData(@RequestParam(required = false) String team_id,
			@RequestParam(required = false) String league_id) {
		return footBallTeamDataService.getTeams("get_teams", team_id, league_id);
	}

	/**
	 * 
	 * @param league_id
	 * 
	 * @throws ParseException GET apiv2.apifootball.com/?action=get_standings
	 * @return standings for leagues included in your current subscription plan
	 */
	@GetMapping(value = "/getStandings")
	public Standings[] getStandings(@RequestParam(required = true) String league_id) throws ParseException {
		return footBallTeamDataService.getStandings("get_standings", league_id);
	}

	public  void test() {
		List<String> test = new ArrayList<>();
		test.add("India East");
		test.add("India West");
		test.add("America East");
		test.add("AMeria West");
		test.add("Koria East");
		test.add("Koria South");

		List<String> test2 = test.stream().filter(ele -> ele.contains("East")).map(ele -> ele.toUpperCase()).
		collect(Collectors.toList());
		//test2.stream().flatMap(null)
	}

}
