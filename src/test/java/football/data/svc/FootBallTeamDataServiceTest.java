package football.data.svc;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.tomcat.util.json.ParseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import football.data.svc.model.CountryData;
import football.data.svc.model.FootBallRequestParam;
import football.data.svc.model.Standings;
import football.data.svc.service.FootBallTeamDataService;
import football.data.svc.utils.RestClientAction;

@SpringBootTest
class FootBallTeamDataServiceTest {

	private FootBallTeamDataService footBallTeamDataService;

	RestClientAction restClient;

	@Mock
	RestTemplate restTemplate;

	@Test
	public void testGetContrySuccess() throws URISyntaxException {
		RestTemplate restTemplate = new RestTemplate();
		final String baseUrl = "https://apiv2.apifootball.com/";
		URI uri = new URI(baseUrl);
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("action", "get_countries");
		headers.add("APIkey", "9bb66184e0c8145384fd2cc0f7b914ada57b4e8fd2e4d6d586adcc27c257a978");
		HttpEntity<CountryData> requestEntity = new HttpEntity<>(null, headers);
		try {
			ResponseEntity<String> data = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, String.class);
			Assertions.assertTrue(data.getBody() != null);
			Assertions.assertEquals(200, data.getStatusCodeValue());

		} catch (HttpClientErrorException ex) {
			Assertions.assertEquals(400, ex.getRawStatusCode());
		}
	}

	@Test
	public void testGetContries() throws URISyntaxException {
		CountryData[] data = footBallTeamDataService.getCountryData("get_countries", null);
		Assertions.assertTrue(data != null);
		Assertions.assertEquals("England", data[0].getCountry_name());
	}

	@SuppressWarnings("deprecation")
	@BeforeEach
	public void buildTeamData() {
		CountryData team = new CountryData();
		paramReq = new FootBallRequestParam();
		footBallTeamDataService = new FootBallTeamDataService(new RestClientAction(new RestTemplate()));
		MockitoAnnotations.openMocks(this);
		team.setCountry_id("41");
		team.setCountry_name("England");
		team.setCountry_logo("https://apiv2.apifootball.com/badges/logo_country/41_england.png");
	}

	FootBallRequestParam paramReq = null;

	@Test
	public void testGetSTandings() throws ParseException {
		paramReq.setAction("get_standings");
		Standings standing = new Standings();
		standing.setCountry_id("41");
		standing.setCountry_name("England");
		standing.setTeam_id("2626");
		standing.setTeam_name("Manchester City");
		standing.setLeague_id("148");
		standing.setLeague_name("Premier League");
		standing.setOverall_league_position("1");
		List<Standings> expectedList = new ArrayList<>();
		expectedList.add(standing);
		Standings[] data = (Standings[]) footBallTeamDataService.getStandings("get_standings", "148");
		assertEquals("41", data[0].getCountry_id());
		assertEquals(expectedList.get(0).getTeam_id(), data[0].getTeam_id());

		assertEquals(expectedList.get(0).getLeague_id(), data[0].getLeague_id());

		assertEquals(expectedList.get(0).getOverall_league_position(), data[0].getOverall_league_position());

		assertEquals(expectedList.get(0).getLeague_name(), data[0].getLeague_name());

	}
	@Test
	public void testGetSTandingswithNullAction() throws ParseException {
		Standings[] data = (Standings[]) footBallTeamDataService.getStandings(null, "148");
		assertEquals(null, data);
	}

}
