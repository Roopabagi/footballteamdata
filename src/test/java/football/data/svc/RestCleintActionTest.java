package football.data.svc;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import football.data.svc.model.CountryData;
import football.data.svc.model.FootBallRequestParam;
import football.data.svc.utils.RestClientAction;


@SpringBootTest
public class RestCleintActionTest {

	RestClientAction restCleinAction;

	FootBallRequestParam paramReq = null;

	List<CountryData> cDataArr;

	@Test
	public void testGetContries() throws NullPointerException {
		paramReq.setAction("get_countries");
		CountryData[] data = (CountryData[]) restCleinAction.execute(paramReq);
		assertEquals(cDataArr.get(0).getCountry_id(), data[0].getCountry_id());

	}

	@BeforeEach
	public void setup() {
		restCleinAction = new RestClientAction(new RestTemplate());
		paramReq = new FootBallRequestParam();

		CountryData cData = new CountryData();
		cDataArr = new ArrayList<>();
		cData.setCountry_id("41");
		cData.setCountry_name("England");
		cData.setCountry_logo("");
		cDataArr.add(cData);
	}

}
