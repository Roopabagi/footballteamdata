FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} football.data.svc.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/football.data.svc.jar"]

